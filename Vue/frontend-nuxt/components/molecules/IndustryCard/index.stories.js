import {
  storiesOf
} from '@storybook/vue'
import {
  text,
  files
} from '@storybook/addon-knobs'
import ServingCard from './index.vue'

storiesOf('Molecules | Serving Card ', module)
  .addDecorator(() => ({
    template: `
      <v-container>
        <v-layout>
          <v-flex xs4>
            <story/>
          </v-flex>
        </v-layout>
      </v-container>`
  }))
  .add('Default', () => ({
    components: {
      ServingCard
    },
    props: {
      icon: {
        default: files('Icon', 'png,jpg', '')
      },
      heading: {
        default: text('Heading', 'Hospitals')
      },
      subHeading: {
        default: text('Sub Heading', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit')
      }
    },
    template: `
    <ServingCard 
      :icon="this.icon[0]"
      :heading="this.heading"
      :subHeading="this.subHeading" />
    `
  }))

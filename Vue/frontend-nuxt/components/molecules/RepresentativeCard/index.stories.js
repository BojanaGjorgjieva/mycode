import {
  storiesOf
} from '@storybook/vue'
import {
  text
} from '@storybook/addon-knobs'
import FeaturedProjectCard from './index.vue'

storiesOf('Molecules | Featured Projects Card ', module)
  .add('Default', () => ({
    components: {
      FeaturedProjectCard
    },
    props: {
      bgImage: {
        default: text('Background Image', 'https://source.unsplash.com/random')
      },
      headline: {
        default: text('Headline', '49ers Stadium')
      },
      subHeadline: {
        default: text('subHeadline', 'Offices and Data Centers')
      },
      full_slug: {
        default: text('slug', 'http://google.com')
      }
    },
    template: `
      <featured-project-card 
        :bgImage="this.bgImage" 
        :headline="this.headline" 
        :subHeadline="this.subHeadline" 
        :slug="this.full_slug" />`
  }))

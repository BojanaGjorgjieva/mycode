import {
  storiesOf
} from '@storybook/vue'
import {
  text,
  files
} from '@storybook/addon-knobs'
import Testimonial from './index.vue'

storiesOf('Molecules | Testimonial ', module)
  .addDecorator(() => ({
    template: `
      <v-container>
        <v-layout>
          <v-flex xs4>
            <story/>
          </v-flex>
        </v-layout>
      </v-container>`
  }))
  .add('Default', () => ({
    components: {
      Testimonial
    },
    props: {
      icon: {
        default: files('Icon', 'png,jpg', '')
      },
      heading: {
        default: text('Heading', 'Hospitals')
      },
      subHeading: {
        default: text('Sub Heading', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit')
      },
      description: {
        default: text('Description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit')
      }
    },
    template: `
    <Testimonial 
      :profilePic="this.icon[0]"
      :name="this.heading"
      :title="this.subHeading"
      :description="this.description" />
    `
  }))

import {
  storiesOf
} from '@storybook/vue'
import {
  text, files
} from '@storybook/addon-knobs'
import FeaturedProductCard from './index.vue'

storiesOf('Molecules | Featured Products Card ', module)
  .add('Default', () => ({
    components: {
      FeaturedProductCard
    },
    props: {
      name: {
        default: text('Name', 'Louvers')
      },
      icon: {
        default: files('Icon', 'png,jpg', '')
      }
    },
    template: `
      <featured-product-card :icon="this.icon[0]" :name="this.name" />
    `
  }))

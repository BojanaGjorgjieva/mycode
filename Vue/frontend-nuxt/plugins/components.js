// import Vue from 'vue'

// import Hero from '~/components/molecules/Hero'
// import Page from '~/components/pages/Page'
// import ContactPage from '~/components/pages/contact'
// import ThankyouPage from '~/components/pages/thank-you'
// import FreeConsultationPage from '~/components/pages/free-consultation'

// // // import ServingCard from '~/components/molecules/IndustryCard'
// // // import Testimony from '~/components/molecules/Testimonial'
// // import AwvGallerySlideShow from '~/components/atoms/AwvGallerySlideShow'
// import FeaturedProducts from '~/components/organisms/FeaturedProducts'
// import FeaturedProjects from '~/components/organisms/FeaturedProjects'
// import WhoWeAreServing from '~/components/organisms/WhoWeAreServing'
// import Testimonials from '~/components/organisms/Testimonials'
// // import ValueMagnet from '~/components/organisms/ValueMagnet'
// import PartnersSection from '~/components/organisms/PartnersSection'
// import TimeLines from '~/components/organisms/TimeLines'
// import MasonGallery from '~/components/organisms/MasonGallery'
// // import CarouselAlbum from '~/components/organisms/CarouselAlbum'
// import FeaturedCaseStudy from '~/components/organisms/FeaturedCaseStudy'
// import FeaturedCaseStudies from '~/components/organisms/FeaturedCaseStudies'
// import RepresentativeCards from '~/components/organisms/RepresentativeCards'
// import CaseStudiesSection from '~/components/templates/CaseStudiesSection'
// import FeaturedBlogPosts from '~/components/templates/FeaturedBlogPosts'
// import Global from '~/components/organisms/Global'

// import AwvStory from '~/components/molecules/AwvStory'

// // // import Page from '~/components/pages/Page.vue'

// Vue.component('hero', Hero)
// Vue.component('featured_products', FeaturedProducts)
// Vue.component('featuredProjects', FeaturedProjects)
// Vue.component('who-we-are-serving', WhoWeAreServing)
// // // Vue.component('serving-card', ServingCard)
// // Vue.component('awv-gallery-slideshow', AwvGallerySlideShow)
// Vue.component('page', Page)
// Vue.component('contact_block', ContactPage)
// Vue.component('thank-you', ThankyouPage)
// Vue.component('free_consultation', FreeConsultationPage)
// Vue.component('partners-section', PartnersSection)
// Vue.component('timelines', TimeLines)
// Vue.component('mason-gallery', MasonGallery)
// // Vue.component('carousel-album', CarouselAlbum)
// Vue.component('o_featured_case_study_card', FeaturedCaseStudy)
// Vue.component('o_featured_case_study_cards', FeaturedCaseStudies)
// Vue.component('o_representative_cards', RepresentativeCards)
// // // Vue.component('testimony', Testimony)
// Vue.component('testimonials', Testimonials)
// Vue.component('case-studies-section', CaseStudiesSection)
// Vue.component('featured-blog-posts-section', FeaturedBlogPosts)
// // Vue.component('value-magnet', ValueMagnet)
// Vue.component('awv_story', AwvStory)
// Vue.component('Global', Global)
import Vue from 'vue'

// // import Page from '~/components/pages/Page.vue'

Vue.component('paragraph', () => import('~/components/atoms/Paragraph'))
Vue.component('heading', () => import('~/components/atoms/Heading'))
Vue.component('bullet_list', () => import('~/components/atoms/bulletList'))

Vue.component('hero', () => import('~/components/molecules/Hero'))
Vue.component('featured_products', () => import('~/components/organisms/FeaturedProducts'))
Vue.component('featuredProjects', () => import('~/components/organisms/FeaturedProjects'))
Vue.component('who-we-are-serving', () => import('~/components/organisms/WhoWeAreServing'))
// // Vue.component('serving-card', ServingCard)
// Vue.component('awv-gallery-slideshow', AwvGallerySlideShow)
Vue.component('page', () => import('~/components/pages/Page'))
Vue.component('contact_block', () => import('~/components/pages/contact'))
Vue.component('thank-you', () => import('~/components/pages/thank-you'))
Vue.component('free_consultation', () => import('~/components/pages/free-consultation'))
Vue.component('partners-section', () => import('~/components/organisms/PartnersSection'))
Vue.component('timelines', () => import('~/components/organisms/TimeLines'))
Vue.component('mason-gallery', () => import('~/components/organisms/MasonGallery'))
// Vue.component('search-dialog', () => import('~/components/molecules/SearchDialog'))
// Vue.component('carousel-album', () => import('~/components/organisms/CarouselAlbum'))
Vue.component('o_featured_case_study_card', () => import('~/components/organisms/FeaturedCaseStudy'))
Vue.component('o_featured_case_study_cards', () => import('~/components/organisms/FeaturedCaseStudies'))
Vue.component('o_representative_cards', () => import('~/components/organisms/RepresentativeCards'))
Vue.component('carousel-album', () => import('~/components/organisms/CarouselAlbum'))
// // Vue.component('testimony', Testimony)
Vue.component('testimonials', () => import('~/components/organisms/Testimonials'))
Vue.component('case-studies-section', () => import('~/components/templates/CaseStudiesSection'))
Vue.component('featured-blog-posts-section', () => import('~/components/templates/FeaturedBlogPosts'))
// Vue.component('value-magnet', ValueMagnet)
Vue.component('awv_story', () => import('~/components/molecules/AwvStory'))
Vue.component('Global', () => import('~/components/organisms/Global'))
Vue.component('sectionHeading', () => import('~/components/molecules/SectionHeading'))
